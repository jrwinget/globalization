---
title: "Globalization & Obesity"
subtitle: "Part I"
author: "PSYC 100, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      slideNumberFormat: ""
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_dark(
  base_color = "#f29724",
  text_color = "#FFFFFF",
  background_color = "#404040",
  title_slide_text_color = "#404040",
  colors = c(
    red = "#FF0000",
    blue = "#00BFFF",
    purple = "#FF00FF"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

# Globesity

<br>
<br>
.center[<iframe width="560" height="315" src="https://www.youtube.com/embed/E5pMAMPXF-k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>]

???

+ Still rapidly increasing: 
  + Nearly 500 million adult were obese in 2008
  + 520 million obese adults in 2013
+ Burden is highest in low-income & middle-income countries
  + Less health-care resources
  + Need to manage coexisting burden of undernutrition
  + However, in most LMICs, the percent that are overweight exceeds those underweight
+ And this global rise in obesity over the past several decades is pretty astounding...and doesn’t seem to be tapering off
+ With the continuation of these trends...and all of the health risks that are either associated or directly caused by obesity...it is estimated in the next few years that obesity will be the single biggest cause of human death on the planet
+ As obesity is on the way to being the single biggest health risk on earth...it is essential that we become more familiar with this growing epidemic...the possible causes and consequences...and what we can do to combat the rise of obesity

---
# Obesity trends among US adults <br> .small-text[BRFSS, 2004]

.center[<img src="assets/img/image01.png" width=90%>]

???

+ What I have here are the CDC Adult Obesity Prevalence Maps for the United States
+ What this map is showing is all self-reported adult obesity prevalence by state
+ These data come from  the Behavioral Risk Factor Surveillance System...which is an on-going state-based, telephone interview survey conducted by CDC and state health departments
+ So BRFSS stands for Behavioral Risk Factor Surveillance System...which is basically just a federal system that is part of the CDC...and they conduct telephone-bases surveys that collect health-related information
+ And we're looking at the 2004 data here
+ As you can see...most of the country self-reports an obesity percentage of 20-24%
+ Keep in mind, this is self-reported...so this is the person's own assessment of their obesity...which means these numbers do not reflect ACTAUL obesity rates...they reflect self-reported obesity rates, which can be under- or overinflated
+ There are about 9 or so states that report obesity rates of over 25%...but a couple also estimate between 15-19%

---
# Obesity trends among US adults <br> .small-text[BRFSS, 2005]

.center[<img src="assets/img/image02.png" width=90%>]

???

+ The next year...more states move to the 25-29% bracket
+ And for the first time...we see states showing more than 30% of their citizens self-reporting their BMI was over 30
+ So when people are self-reporting they are obese in these studies...they are saying they have a BMI of at least 30 or more
+ This roughly corresponds to being about 30 pounds overweight for a person who is 5'4"
+ And so from 2004 to 2005...we see a slight increase in the prevalence of obesity

---
# Obesity trends among US adults <br> .small-text[BRFSS, 2006]

.center[<img src="assets/img/image03.png" width=90%>]

???

+ The next year...a few states moved down from the over 30% prevalence rate to the 25-29% prevalence rate
+ But again...we see more states move from the 20-24% range to the 25-29% range
+ So still an overall increase in the prevalence of people self-reporting a BMI of 30 or higher

---
# Obesity trends among US adults <br> .small-text[BRFSS, 2007]

.center[<img src="assets/img/image04.png" width=90%>]

???

+ Then, in 2007, we see a pretty big increase again

---
# Obesity trends among US adults <br> .small-text[BRFSS, 2008]

.center[<img src="assets/img/image05.png" width=90%>]

???

+ And this continues on for the next couple years

---
# Obesity trends among US adults <br> .small-text[BRFSS, 2009]

.center[<img src="assets/img/image06.png" width=90%>]

???

+ And by 2009...9 states self-reported a prevalence of 30% or greater
+ And an additional 26 states self-reported prevalence between 25-29% 

---
# Obesity trends among US adults <br> .small-text[BRFSS, 1990, 1999, 2009]

.center[<img src="assets/img/image07.png" width=90%>]

???

+ This is even clearer to when we place some of these graphs side by side
+ In 1990...virtually every state that provided data self-reported obesity prevalence rates of less than 14%...with only one or two reporting rates between 15-19%
+ 9 years later...only a handful of states self-reported prevalence rates between 10-14%
+ By 1999....most states had prevalence rates between 15-19%...and just about as many reported rates between 20-24%
+ And this dramatically changed yet again by the next decade
+ By 2009...only Colorado reported rates less than 20 percent...while most states reported rates of 25$ and greater
+ Other data by the CDC shows that by 2010...no state had a prevalence of obesity less than 20% (Center for Disease Control and Prevention, 2012)
+ By the year 2025...researchers estimate that up to half of the population of the US will be obese (Treacy, 2005)

---
# A truly global problem

.center[<img src="assets/img/image08.jpg" width=100%>]

???

+ This map is a visual of the PREVALENCE of obesity in each country across the world
+ The percentage of the population in each country that is OBESE 
+ Red is greater than 30 percent...orange is 20-29%

---
# A truly global problem

.center[
<img src="assets/img/image09.png" width=100%>
Raw # of people in millions 
]

???

+ And then...as a comparison...this graph shows the Raw # of people in millions who are obese in each country
+ It’s important to look at data both in percentages of the total population...like the maps we've been looking at...but it's also important to look at the raw #’s of people
  + For example...on the other map...India didn’t look like it had much trouble with obesity rates...they are less than 10%
  + But if we look at the raw # of people who are obese in India...we can see that the country has a huge number of obese people...84.3 Million!
  + Large countries with low percentages of rates of obesity could still have a huge number of individuals suffering from this disease with very limited infrastructure in many instances
+ Also, you can see on this map that for both prevalence and raw numbers of people per country...the US takes the cake...literally...US here in purple
+ But Globesity is in no sense limited to the US...it is truly a global problem

---
# Globalization & obesity

+ Obesity has risen dramatically at the same time as globalization has surged

<br>
.center[<img src="assets/img/image10.png">]

<br>
<br>
.right[*Malik et al. (2013)*]

???

+ Obesity has risen dramatically at the same time as globalization has surged
+ Because of this...globalization and obesity seem like they could be related
+ But since correlation does NOT equal causation...we have to look into the relationship further
  + These things are complex and intertwined...so we have to try to isolate the different sources of influence to try to get a clearer picture of their relationship
+ The Malik article has a very nice model that captures what the key drivers and factors are in this link
+ Key way to think about these is that macro-level factors influence individual behaviors...and these individual behaviors make up the micro level
+ The idea is that these 3 macro-level factors...free trade, economic growth, and urbanization...fuel dramatic changes in built environments, food availability, physical activity related to labor 
+ These changes...as well as other larger factors...influence what are called obesogenic behaviors
+ Obesogenic behaviors are thinks like diet, physical activity, sleep and lifestyles
+ These these changes influence obesogenic behaviors that promote positive energy imbalance
  + Do people know what I mean when I say positive energy imbalance?
  + It's when a person’s caloric intake is more than the calories they burn in a day
  + Basically...we eat more than we can burn off...which leads to weight gain
+ This positive energy imbalance leads to obesity...and this is spurring on the obesity epidemic...especially in low- and middle-income countries
+ So to recap....globalization leads to these 3 macro factors here...free trade, economic growth, and urbanization
  + These lead to individual-level changes in behavior...which lead to positive energy imbalance...which leads to obesity

---
## Macro-level factors: <br> Free trade (Malik et al., 2013)

+ LMICs enter free-trade agreements with the US
  + 64% higher levels of sugar-sweetened beverage intake


+ US Farm bill – affects pricing
  + Prioritized corn and soy which feed livestock
  + Limited subsidies for fruits and vegetable production


+ Unregulated capitalistic food system
  + Declining cost of food & movement towards mass preparation

???

+ The 1st macro level factor they talk about is global trade liberalization...or which is just a fancy way of saying free trade
+ Between the 1970s and 1990s...many countries underwent economic adjustments...which included more free trade policies
+ These policies have altered the food supply and had direct effects on the obesity epidemic
+ Free trade can affect the availability of certain foods by enabling the trade of greater amounts and varieties of food...by removing barriers to foreign investment in food distribution...and through expansion of multinational food companies and fast-food chains
+ Some research as shown that as LMICs enter free-trade agreements with the US...they have a 63.4% higher levels of sugar-sweetened beverage consumption per capita
+ Here in the US...the farm bill has influence the cost and availability of foods and crops
+ The farm bill prioritized subsidies for corn and soy because they are the raw ingredients for most food and they are the primary feed for livestock
+ This has also helped keep prices low for meat and poultry
+ However...the farm fill provided little support for fruit and vegetable production...which is why we see these healthier foods often sold at higher prices
+ Others have also pointed out that the declining cost of food and the shift towards mass preparation of foods has been a major contributor to the obesity epidemic in the USA...which can also be applied to other countries experiencing similar changes in food availability and choice
+ Researchers suggest a partial cause of the obesity epidemic in the US is the inevitable consequence of an unregulated capitalistic food system
+ Basically...companies are competing with each other to us consumers to eat more of their product

---
## Macro-level factors: <br> Economic growth (Malik et al., 2013)

+ Increase in income (~2%/year) – more rapid in LMICs
  + Increases lead to changes in diet, activity


+ Unlike in the US, higher incomes in many LMICs positively associated with BMI (more income = higher BMI)


+ Thus, the association between individual/familial SES and BMI varies on the economic development of the country

???

+ Next macro-level factor is economic growth...the heading in the Malik article is Income and SES
+ The idea here is that over the next few decades...global per capita income is projected to rise at a rate of >2% per year
+ In LMICs...this increase is expected to be even more rapid
+ The prevalence of obesity positively correlates with early stages of economic growth and development
+ So as populations undergo economic growth...obesity tends to increase early on because they undergo nutritional and lifestyle transitions...but they also tend to have little access to health services and education
+ So there are these changes in their diet and activities....but it takes a little while for health services and health education to make their way to the people to inform them of healthier behaviors
+ So that's sort of between countries
+ When we look within countries...we see that obesity tends to be related to SES
+ In many LMICs...body weight is positively associated with SES...which contrasts with what we see in data from the US and other high-income countries
+ In the US...BMI tends to be negatively correlated with SES
+ However, the association between SES and body weight is thought to depend on the level of economic development in the country
+ There is some evidence that shows obesity shifted towards low SES groups as a country's Gross National Product increased

---
## Macro-level factors: <br> Urbanization (Malik et al., 2013)

+ Rapid shift to urbanization with more people now living in cities (since 2008)
  + Urbanization process more pronounced among LMICs


+ Urbanized environment leads to...
  + changes in built living environment
  + range of food options available
  + more mechanized, technologically-driven way of life
      + BUT, may be more access to care

???

+ The third macro-level factor is urbanization
+ The proportion of the world’s population living in urban areas has increased markedly, from 13% in 1900 to almost 50% in 2005
+ In 2008...we saw for the first time in history...more people living in urban environments than rural ones
+ This trend is expected to continue...primarily in countries where the vast majority of the population is currently rural
+ Urban sprawl...the outward spreading of a city and its suburbs...has been associated with obesity in the USA
+ And this is already occurring in some LMICs as urban areas are starting to expand
+ There are many consequences of urbanization on obesity...and they occur largely as a result of changes in the built living environment...the range of food options available...and through lifestyle changes like technological advancement 
+ The shift towards a more technologically driven way of life has led to more sedentary lifestyles and spending less energy...tech can do a lot for us now
+ And expending low amounts energy decreases energy requirements...which allows excess calories to build up quickly
+ So these trends of positive energy balance are expected to continue as urbanization continues
+ At the same time...however...urbanization facilitates greater access to health care and education...which can have beneficial effects on obesity
+ [pause]
+ So those are the 3 macro-level factors Malik et al. point out...free trade...economic growth...and urbanization

---
## Micro-level factors (Malik et al., 2013)

+  Physical activity
  + Work, transportation, household, and leisure changes – less PA!


+ Sleep
  + Urbanization = poorer sleep in cities


+ Nutrition/Diet (e.g., nutrition transition) 
  + Increase in fast food outlets & consumption
  + Large-scale food markets & marketing
  + Sugar-sweetened beverages intake increases 

???

+ And according to Malik et al...these macro-level factors influence micro-level factors
+ And these micro-level factors are our individual behaviors
+ They discuss 3 main micro-level factors...physical activity..sleep...and nutrition/diet
+ In terms of physical activity...densely populated areas with little outdoor recreational space limit the opportunities for walking and leisure-time physical activity
+ Data from the US show that levels of physical activity for leisure have been fairly stable...and in some cases, have actually increased...over the past 50 years
+ However, levels of physical activities related to work, transportation, and household chores have declined dramatically...and sedentary behaviors such as watching TV and spending time online...or sitting in front of a screen in general...have increased substantially...which has led to an overall reduction in physical activity
+ In many LMICs...there has been a shift away from jobs with high-energy expenditure...such as farming, mining, and forestry...towards jobs in more sedentary sectors...such as manufacturing, services, and office-based work...which is often typical of countries experiencing economic transition and urbanization
+ Certain behavioral changes brought about by urban living could also contribute to the development of obesity in LMICs
+ Urbanization is associated with a decrease in sleep duration
+ This is because noise pollution...street and domestic lighting...access to television and the internet...shift working...and night-time social activities are more common in urban areas than in rural areas
+ And short sleep duration has been associated with weight gain in both children and adults in developed countries, such as the US
+ There have also been dramatic changes to the range of food options available
+ As many countries experience rapid economic growth and changes to food choice and availability brought about by urbanization...shifts in diets occur that promote overnutrition and positive energy balance
+ More and more fast food outlets have opened worldwide
  + For example, the global number of McDonald’s grew from 951 in 1987 to 7,135 in 2002
+ And we know Fast food is linked to obesity and cardiometabolic disease for many reasons
+ Another important shift has been the increase in multinational, regional, and large local supermarkets...which are rapidly displacing fresh food markets and farm shops
+ The effect of supermarket use on diet quality and obesity can be substantial...they are a source of highly processed foods, high-energy snack foods, sweets and sugary beverages...all of which lead to increased obesity
+ Also...in the past three to four decades...consumption of sugar-sweetened beverages has dramatically increased across the globe
+ Sugar-sweetened beverages promote weight gain because they don't really make us feel full
+ And because of this...when we consume calories in liquid form...we don't compensate for them later by reducing the amount of food we eat during our next meal
+ Instead...we tend to eat the same amount we normally would have...except we have the additional calories from the sugary drink now too...which leads to increased energy intake and positive energy balance
+ But it's tough to break this habit right...especially because soda and other sugary drinks tend to be cheaper than water
+ Does anyone know why this is?
